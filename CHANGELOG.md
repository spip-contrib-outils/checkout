# Changelog

## 2.1.0

### Added

- !14 Visuellement mieux indiquer les dépots téléchargés ou mis à jour

## 2.0.0

### Changed

- !13 N’ajoute plus `v` automatiquement en demandant un tag.


## 1.9.1

### Fixed

- correction de l’option --forcerm
- correction pour Windows

## 1.9.0

- Ne pas chercher de plugins-dist en SPIP 5-dev (composer install suffit)
- Vérifier que la référence git (branche, commit ou tag) existe avant de checkout dessus
- Si une référence 'spip' n’existe pas, ne pas tenter de checkout les plugins-dist

## 1.8.0

- On lance composer install sur la commande `spip` (pour SPIP 4.2+)

## 1.7.1

- Le script nettoie les branches et tags qui ne sont plus présents sur origin (fetch --prune --prune-tags)

## 1.7.0
- On utilise la clé 'tag' dans plugins-dist.json si présent.

## 1.6.0

- On utilise la clé 'branch' dans plugins-dist.json si présent.

## 1.5.0
- On prend prioritairement pour 'checkout spip' un fichier plugins-dist.json s’il est présent.

## 1.4.0

- commande 'checkout' de préférence (checkout.php continue de fonctionner)
- adaptation aux changements des branches et tags SPIP / plugins-dist du 27 09 2020
- on met le git de SPIP en minuscule par défaut, mais on tolère l’ancien nommage de l’url.